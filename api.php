<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$mode = isset($_POST['mode']) ? $_POST['mode'] : 'deleteColumns';
	$delimiter = isset($_POST['delimiter']) ? $_POST['delimiter'] : 'tabulation';
	
	if($mode == 'showTable') {
		$tableText = isset($_POST['tableText']) ? $_POST['tableText'] : '';
	}
	elseif($mode == 'swapColumns') {
		$tableText = isset($_POST['tableText']) ? $_POST['tableText'] : '';
		$swapColumn1 = isset($_POST['swapColumn1']) ? $_POST['swapColumn1'] : 1;
		$swapColumn2 = isset($_POST['swapColumn2']) ? $_POST['swapColumn2'] : 1;
	}
	elseif($mode == 'deleteColumns') {
		$tableText = isset($_POST['tableText']) ? $_POST['tableText'] : '';
		$deleteColumn = isset($_POST['deleteColumn']) ? $_POST['deleteColumn'] : 1;
	}
	
	include_once 'TableProcessor.php';
	TableProcessor::loadLocalization($localization);
	
	$msg = '';
	$TableProcessor = new TableProcessor();
	$TableProcessor->setDelimiter($delimiter);
	if($mode == 'showTable') {
		$TableProcessor->setText($tableText);
		$TableProcessor->run();
		$TableProcessor->saveCacheFiles();
		$tableText = '<input type="hidden" name="tableText" value="' . htmlspecialchars($TableProcessor->getTableText()) . '">';
	}
	elseif($mode == 'swapColumns') {
		$TableProcessor->swapColumns($tableText, $swapColumn1-1, $swapColumn2-1);
		$tableText = '<input type="hidden" name="tableText" value="' . $TableProcessor->getTableText() . '">';
	}
	elseif($mode == 'deleteColumns') {
		$TableProcessor->deleteColumn($tableText, $deleteColumn-1);
		$tableText = '<input type="hidden" name="tableText" value="' . $TableProcessor->getTableText() . '">';
	}
	$tableArray = $TableProcessor->getTableArray();
	$columnsMax = $TableProcessor->getColumnsMax();
	$functionality = '';
	if(!empty($tableArray)) {
		$selector = '';
		for($i = 1; $i <= $columnsMax; $i++) {
			$selector .= "<option value='$i'>" . TableProcessor::showMessage('column') . " №$i</option>";
		}
		$functionality .= '<br/><span class="bold">' . TableProcessor::showMessage('swap columns') . '</span>';
		$functionality .= '&nbsp;<select id="swapColumn1Id" name="swapColumn1" class="selector-primary">' . $selector . '</select>';
		$functionality .= '&nbsp;<select id="swapColumn2Id" name="swapColumn2" class="selector-primary">' . $selector . '</select>';
		$functionality .= '&nbsp;<button type="button" id="SwapColumnsId" name="SwapColumns" class="button-primary">' . TableProcessor::showMessage('swap columns button') . '</button>';
		$functionality .= '<br /><span class="bold">' . TableProcessor::showMessage('delete column') . '</span>';
		$functionality .= '&nbsp;<select id="deleteColumnId" name="deleteColumn" class="selector-primary">' . $selector . '</select>';
		$functionality .= '&nbsp;<button type="button" id="DeleteColumnsId" name="DeleteColumns" class="button-primary">' . TableProcessor::showMessage('delete column button') . '</button>';
	}
	
	$result['tableText'] = $tableText;
	$result['tableHtml'] = $TableProcessor->getTableHtml();
	$result['functionality'] = $functionality;
	$msg = json_encode($result);
	
	echo $msg;
?> 