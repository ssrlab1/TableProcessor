<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'TableProcessor.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = TableProcessor::loadLanguages();
	TableProcessor::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo TableProcessor::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<style>
			table.processing {
				border-spacing:0.1em;
				margin-bottom:1em;
				margin-top:1em
			}
			table.processing td {
				border:1px solid #143d51;
				padding:0.1em 1em
			}
			table.processing thead td {
				background-color:#c1e0ef;
				font-weight:bold;
				text-align:center;
				vertical-align:middle
			}
			table.processing thead td.curcol {
				background-color:#3399CC;
				color:#FFFFFF
			}
			table.processing a {
				text-decoration: none
			}
		</style>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', TableProcessor::showMessage('default input'))); ?>";
			function InsertCodeInTextArea(textAreaId, textValue) {
				//Get textArea HTML control 
				var txtArea = document.getElementById(textAreaId);
				//IE
				if (document.selection) {
					txtArea.focus();
					var sel = document.selection.createRange();
					sel.text = textValue;
					return;
				}
				//Firefox, chrome, mozilla
				else if (txtArea.selectionStart || txtArea.selectionStart == '0') {
					var startPos = txtArea.selectionStart;
					var endPos = txtArea.selectionEnd;
					var scrollTop = txtArea.scrollTop;
					txtArea.value = txtArea.value.substring(0, startPos) + textValue + txtArea.value.substring(endPos, txtArea.value.length);
					txtArea.focus();
					txtArea.selectionStart = startPos + textValue.length;
					txtArea.selectionEnd = startPos + textValue.length;
				}
				else {
					txtArea.value += textArea.value;
					txtArea.focus();
				}
			}
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', TableProcessor::showMessage('default input'))); ?>";
			$(document).ready(function () {
				$(document).on('click', 'button#ShowTableId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/TableProcessor/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'tableText': $('textarea#inputTextId').val(),
							'delimiter': $('select#delimiterId').val(),
							'mode': 'showTable'
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = '';
							output += result.tableText;
							output += result.tableHtml;
							output += result.functionality;
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
				$(document).on('click', 'button#SwapColumnsId', function() {
					var tableText = $('input:hidden[name=tableText]').val();
					var delimiter = $('select#delimiterId').val();
					var swapColumn1 = $('select#swapColumn1Id').val();
					var swapColumn2 = $('select#swapColumn2Id').val();
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/TableProcessor/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'tableText': tableText,
							'delimiter': delimiter,
							'swapColumn1': swapColumn1,
							'swapColumn2': swapColumn2,
							'mode': 'swapColumns'
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = '';
							output += result.tableText;
							output += result.tableHtml;
							output += result.functionality;
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
				$(document).on('click', 'button#DeleteColumnsId', function() {
					var tableText = $('input:hidden[name=tableText]').val();
					var delimiter = $('select#delimiterId').val();
					var deleteColumn = $('select#deleteColumnId').val();
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/TableProcessor/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'tableText': tableText,
							'delimiter': delimiter,
							'deleteColumn': deleteColumn,
							'mode': 'deleteColumns'
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = '';
							output += result.tableText;
							output += result.tableHtml;
							output += result.functionality;
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/TableProcessor/?lang=<?php echo $lang; ?>"><?php echo TableProcessor::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo TableProcessor::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo TableProcessor::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = TableProcessor::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . TableProcessor::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="InsertCodeInTextArea('inputTextId', '\u0009');"><?php echo TableProcessor::showMessage('tabulation'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo TableProcessor::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo TableProcessor::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo TableProcessor::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\t', "\t", str_replace('\n', "\n", TableProcessor::showMessage('default input'))); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<select id="delimiterId" name="delimiter" class="selector-primary">
						<option value="0"><?php echo TableProcessor::showMessage('tabulation'); ?></option>
						<option value="1"><?php echo TableProcessor::showMessage('space'); ?></option>
						<option value="2"><?php echo TableProcessor::showMessage('new line'); ?></option>
						<option value="3"><?php echo TableProcessor::showMessage('comma'); ?></option>
					</select>
				</div>
				<div class="col-md-12">
					<button type="button" id="ShowTableId" name="ShowTable" class="button-primary"><?php echo TableProcessor::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo TableProcessor::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo TableProcessor::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/TableProcessor" target="_blank"><?php echo TableProcessor::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/TableProcessor/-/issues/new" target="_blank"><?php echo TableProcessor::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo TableProcessor::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo TableProcessor::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo TableProcessor::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php TableProcessor::sendErrorList($lang); ?>