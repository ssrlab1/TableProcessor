<?php
	class TableProcessor
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $tableText = '';
		private $tableArray = array();
		private $columnsMax = 0;
		private $tableHtml = '';
		private $delimiter = "\t";
		private $delimitersArray = array("\t", " ", "\n", ",");
		const BR = "<br>\n";
		
		function __construct()
		{
			
		}
		
		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Table Processor';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/TableProcessor/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setText($inputText)
		{
			$this->tableText = $inputText;
		}

		public function setDelimiter($inputValue)
		{
			if(!is_numeric($inputValue)) return;
			$inputValue = intval($inputValue);
			if ($inputValue < 0 || $inputValue > 3) return;
			$this->delimiter = $this->delimitersArray[$inputValue];
		}
		
		private function formTableArr()
		{
			$tableArray = array();
			if(!empty($this->tableText))
			{
				$rows = preg_split("/\n/", $this->tableText);
				// get $this->columnsMax;
				foreach($rows as $rowIndex => $row)
				{
					$row = trim($row);
					if(substr($row, 0, 1) != '#' && substr($row, 0, 2) != '//')
					{
						$cells = explode($this->delimiter, $row);
						if(count($cells) > $this->columnsMax)
						{
							$this->columnsMax = count($cells);
						}
					}
				}
				// form $tableArray;
				foreach($rows as $rowIndex => $row)
				{
					if(substr($row, 0, 1) != '#' && substr($row, 0, 2) != '//')
					{
						$cells = explode($this->delimiter, $row);
						for($i = 0; $i < $this->columnsMax; $i++)
						{
							if(isset($cells[$i]))
							{
								$tableArray[$rowIndex][$i] = $cells[$i];
							}
							else
							{
								$tableArray[$rowIndex][$i] = '';
							}
						}
					}
				}
			}
			return $tableArray;
		}
		
		private function formTableHtml()
		{
			$tableHtml = '<table width="100%" class="processing" align="center">';			
			foreach($this->tableArray as $row)
			{
				$tableHtml .= '<tr>';
				for($i = 0; $i < $this->columnsMax; $i++)
				{
					$tableHtml .= '<td>';
					if(isset($row[$i]))
					{
						$tableHtml .= $row[$i];
					}
					$tableHtml .= '</td>';
				}
				$tableHtml .= '</tr>';
			}
			$tableHtml .= '</table>';
			return $tableHtml;
		}
		
		public function run()
		{
			$this->tableArray = $this->formTableArr();
			$this->tableHtml = $this->formTableHtml();
		}
		
		public function swapColumns($tableText, $swapColumn1, $swapColumn2)
		{
			//if($swapColumn1 != $swapColumn2)
			{
				$this->tableText = $tableText;
				$this->tableArray = $this->formTableArr();
				foreach($this->tableArray as &$row)
				{
					if(array_key_exists($swapColumn1, $row) && array_key_exists($swapColumn2, $row))
					{
						$tmp = $row[$swapColumn1];
						$row[$swapColumn1] = $row[$swapColumn2];
						$row[$swapColumn2] = $tmp;
					}
					$tableTextRowsArray[] = join($this->delimiter, $row);
				}
				$this->tableText = join("\n", $tableTextRowsArray);
				$this->tableHtml = $this->formTableHtml();
			}
		}
		
		public function deleteColumn($tableText, $selectedColumn)
		{
			$this->tableText = $tableText;
			$this->tableArray = $this->formTableArr();
			$this->tableText = '';
			foreach($this->tableArray as &$row)
			{
				unset($row[$selectedColumn]);
				$row = array_values($row);
				$tableTextRowsArray[] = join($this->delimiter, $row);
			}
			$this->columnsMax--;
			$this->tableText = join("\n", $tableTextRowsArray);
			$this->tableHtml = $this->formTableHtml();
		}
		
		public function saveCacheFiles()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'TableProcessor';
			$sendersName = 'Table Processor';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "Table Processor from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($new_file, $this->tableText);
			fclose($new_file);
			if(mb_strlen($this->tableText))
			{
				$mail_body .= "Уваходны тэкст пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $this->tableText, $matches);
				$mail_body .= '<i>' . str_replace("\n", self::BR, trim($matches[0])) . '</i>' . self::BR;
			}
			$mail_body .= "Уваходны тэкст цалкам захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=TableProcessor&t=in&f=$filename" . self::BR . self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getTableText()
		{
			return $this->tableText;
		}
		
		public function getTableArray()
		{
			return $this->tableArray;
		}
		
		public function getTableHtml()
		{
			return $this->tableHtml;
		}
		
		public function getColumnsMax()
		{
			return $this->columnsMax;
		}
	}
?>
